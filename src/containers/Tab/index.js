import React, {useEffect} from "react";
import {bindActionCreators} from "redux";
import {connect} from "react-redux";

import {fetchTabByEndpoint} from "../../actions/tabs";
import {setSelectedFontId} from "../../actions/fonts";

import Text from '../../components/Text'
import FontsListing from '../../components/FontsListing'

const Tab = (props) => {
  const {
    activeTab,
    selectedFont,
    content_endpoint,
    setSelectedFontId,
    fetchTabByEndpoint
  } = props;

  const fetchData = () => {
    fetchTabByEndpoint({content_endpoint})
  }

  useEffect(fetchData, [content_endpoint]);

  const handleCardClick = (id) => {
    setSelectedFontId(id)
  }

  if (!activeTab.type) {
    return false
  }

  return (
    <>
      {
        activeTab.type === 'Text' ?
          <Text content={activeTab.content} /> :
          <FontsListing
            {...{activeTab, selectedFont, handleCardClick}}
          />
      }
    </>
  )
}

const mapStateToProps = state => {
  const {
    tabs,
    fonts
  } = state;

  return {
    activeTab: tabs.activeTab,
    selectedFont: fonts.selectedFontId
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  setSelectedFontId,
  fetchTabByEndpoint
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Tab);
