import React from "react";
import styled from 'styled-components';

import FontCard from '../FontCard';

const FontsListing = ({activeTab, handleCardClick, selectedFont}) => {
  return (
    <FontsListingHolder>
      {
        activeTab.content.map((item, index) => {
          return (
            <FontCardHolder key={item.id}>
              <FontCard
                onClick={ handleCardClick }
                cardData={item}
                selectedFont={ selectedFont === item.id }
                largeCard={(index % 4) === 0}
              />
            </FontCardHolder>
          )
        })
      }
    </FontsListingHolder>
  )
}

export default FontsListing;

const FontsListingHolder = styled.div`
  @media (min-width: 768px) {
    &:after {
      content: '';
      clear: both; 
      display: block;
    }
  }
`
const FontCardHolder = styled.div`
  margin-bottom: 15px;
 
  @media (max-width: 767px) {
    width: 100%;
    max-width: 320px;
    
    &:last-child {
      margin-bottom: 0;
    }
  }
  
  @media (min-width: 767px) {
    width: 60%;
    float: left;
    
    &:nth-child(3n + 1) {
      width: 40%;
      padding-right: 15px;
    }
  }
`
