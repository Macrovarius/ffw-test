import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { fetchTabs } from '../../actions/tabs'

import Tabs from '../../components/Tabs'
import styled from "styled-components";

const Home = (props) => {
  const {tabsList} = props;

  const fetchData = () => {
    props.fetchTabs()
  }

  useEffect(fetchData, []);



  return (
    <Wrapper>
      {
        tabsList.length > 0 &&
        <Tabs
          {...{tabsList}}
        />
      }
    </Wrapper>
  )
}

const mapStateToProps = state => {
  const {
    tabs
  } = state;

  return {
    tabsList: tabs.tabsList
  }
};

const mapDispatchToProps = dispatch => bindActionCreators({
  fetchTabs
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);


const Wrapper = styled.div`
  width: 100%;
  max-width: 485px;
  margin: 0 auto;
  padding: 100px 0;
  
  @media (max-width: 767px) {
    padding: 30px 15px;
  }
`
