import React from "react";
import styled from 'styled-components';

const Text = ({content}) => {
  return <TextHolder>{content}</TextHolder>
}

export default Text;

const TextHolder = styled.p`
  display: flex;
  align-items: center;
  min-height: 200px;
  text-align: center;
`
