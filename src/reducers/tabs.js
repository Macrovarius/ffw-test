import {
  SET_TABS,
  SET_ACTIVE_TAB,
} from '../constants/actionTypes';

const initialState = {
  tabsList: [],
  activeTab: {
    type: null
  }
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_TABS:
      return {
        ...state,
        tabsList: payload
      };

    case SET_ACTIVE_TAB:
      return {
        ...state,
        activeTab: payload
      };

    default:
      return state;
  }
};
