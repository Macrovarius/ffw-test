import {
  SET_TABS,
  SET_ACTIVE_TAB
} from '../constants/actionTypes';

import { parseResponse, headers, API_URL } from '../helpers/fetchHelpers'

export const fetchTabs = () => dispatch => {
  return fetch(`${API_URL}/tabs`, {
    method: 'GET',
    headers: {
      ...headers,
    },
  })
    .then(parseResponse)
    .then(payload => dispatch({ type: SET_TABS, payload: payload }))
    .catch(console.log);
};

export const fetchTabByEndpoint = ({content_endpoint}) => dispatch => {
  return fetch(`${API_URL}/${content_endpoint}`, {
    method: 'GET',
    headers: {
      ...headers,
    },
  })
    .then(parseResponse)
    .then(payload => dispatch({ type: SET_ACTIVE_TAB, payload: payload }))
    .catch(console.log);
};
