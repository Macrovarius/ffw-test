import { combineReducers } from 'redux';

import tabs from './tabs'
import fonts from './fonts'

export default combineReducers({
  tabs,
  fonts
})
