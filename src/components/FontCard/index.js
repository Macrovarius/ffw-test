import React from "react";
import styled from 'styled-components'

const FontCard = (props) => {
  const {
    onClick,
    largeCard,
    selectedFont,
    cardData: { id, abbr, label, color },
  } = props;

  const onCardClick = (e) => {
    onClick(+e.currentTarget.dataset.id)
  }

  return (
    <CardHolder
      data-id={id}
      onClick={ onCardClick }
      {...{selectedFont, largeCard}}
    >
      <CardColorBox
        bgColor={color}
        title={props.cardData['color-blind-label']}
        {...{largeCard}}
      >
        <CardAbbr>{abbr}</CardAbbr>
      </CardColorBox>
      <CardLabel>{label}</CardLabel>
    </CardHolder>
  )
}

export default FontCard;

const CardHolder = styled.div`
  display: flex;
  align-items: center;
  width: 100%;
  cursor: pointer;
  opacity: ${props => props.selectedFont ? .5 : 1};
  
  @media (min-width: 768px) {
    flex-direction: ${props => props.largeCard && 'column'};
    align-items: ${props => props.largeCard ? 'flex-start' : 'center'};
  }
`

const CardColorBox = styled.div`
  display: flex;
  align-items: flex-end;
  width: 50px;
  min-width: 50px;
  height: 50px;
  margin-right: 10px;
  border-radius: 6px;
  border: 1px solid gray;
  padding: 5px 10px;
  background-color: ${props => props.bgColor};
  box-shadow: inset 0 0 0 2px #fff;
  
  ${props => props.largeCard && `
    @media (min-width: 768px) {
      margin-bottom: 10px;
      width: 70px;
      min-width: 70px;
      height: 70px;
      margin-right: 0;
    }
  `}
`

const CardAbbr = styled.div`
  font-size: 16px;
  font-weight: 600;
  color: rgba(255, 255, 255, .5);
`

const CardLabel = styled.div`
  position: relative;
  padding-left: 10px;
  
  &:before {
    content: '';
    position: absolute;
    left: 0;
    top: 7px;
    width: 5px;
    height: 5px;
    border-radius: 50%;
    background-color: gray;
  }
`
