import {SET_SELECTED_FONT_ID} from "../constants/actionTypes";

export const setSelectedFontId = (payload) => dispatch => {
  dispatch({ type: SET_SELECTED_FONT_ID, payload: payload })
};
