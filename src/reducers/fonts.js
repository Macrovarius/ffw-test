import {
  SET_SELECTED_FONT_ID
} from '../constants/actionTypes';

const initialState = {
  selectedFontId: '',
};

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SET_SELECTED_FONT_ID:
      return {
        ...state,
        selectedFontId: payload
      };

    default:
      return state;
  }
};
