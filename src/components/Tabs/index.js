import React, { useState } from "react";
import styled from 'styled-components';

import Tab from '../../containers/Tab';

const Tabs = ({ tabsList }) => {
  const [activeTab, setActiveTab] = useState(tabsList[0]);

  return (
    <>
      <TabsHeader>
        <Title>
          Please select one font
        </Title>
        <TabsTitleList>
          {tabsList.map(({id, label, content_endpoint}) => (
            <TabTitle
              key={id}
              active={activeTab.id === id}
              onClick={ () => setActiveTab({id, label, content_endpoint}) }
            >
              {label}
            </TabTitle>
          ))}
        </TabsTitleList>
      </TabsHeader>

      <CurrentTabHolder>
        <Tab
          content_endpoint={activeTab.content_endpoint}
        />
      </CurrentTabHolder>
    </>
  )
};

export default Tabs;

const TabsHeader = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  
  @media (max-width: 767px) {
    flex-direction: column;
  }
`
const Title = styled.div`
  font-size: 24px;
  font-weight: 600;
  
  @media (max-width: 767px) {
    margin-bottom: 10px;
  }
`
const TabsTitleList = styled.div`
  display: flex;
  align-items: center;
`
const TabTitle = styled.div`
  margin-left: 10px;
  font-size: 12px;
  text-transform: uppercase;
  cursor: pointer;
  color: ${props => props.active ? 'orange' : 'lightgray'};
  font-weight: 600;
  
  @media (max-width: 767px) {
    font-size: 16px;
  }
`
const CurrentTabHolder = styled.div`
  margin-top: 10px;
  border-radius: 6px;
  border: 2px solid gray;
  padding: 40px 20px;
  
  @media (max-width: 767px) {
    padding: 10px;
  }
`
