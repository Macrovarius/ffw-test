export const SET_TABS = 'SET_TABS';
export const SET_ACTIVE_TAB = 'SET_ACTIVE_TAB';
export const SET_SELECTED_FONT_ID = 'SET_SELECTED_FONT_ID';
